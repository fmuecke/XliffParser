﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xlflib
{
    //tool-id="MultilingualAppToolkit" tool-name="Multilingual App Toolkit" tool-version="3.1.1250.0" tool-company="Microsoft"
    public class XlfTool
    {
        public string Id { get; }
        public string Name { get; }
        public string Version { get; }
        public string Company { get; }
    }
}
